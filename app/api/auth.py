from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response


class ViewEnabledAuth(Object):
	authentication_classes = (
		SessionAuthentication,
		BasicAuthentication
	)
    permission_classes = (IsAuthenticated, )