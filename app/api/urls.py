from django.conf.urls import url, include

urlpatterns = [
	url(r'^bots', include('bots.api.urls'), name='bots'),
]