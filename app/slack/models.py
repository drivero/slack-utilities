from django.db import models


class Channel(models.Model):
	name = models.CharField(max_length=75)

	def __str__(self):
		return self.name


class Message(models.Model):
	message = models.TextField()
	date = models.DateTimeField(auto_now=True)
	channel = models.ForeignKey(Channel, related_name='messages')

	def __str__(self):
		return self.message
