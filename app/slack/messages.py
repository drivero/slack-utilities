from slackclient import SlackClient


class MessageHandler():
	REALTIME_MESSAGING_API = 'api.rtm.start'
	SEND_MESSAGE_API = 'chat.postMessage'

	def __init__(self, token):
		self.client = SlackClient(token)

	def send_message(self, message, user):
		self.client.api_call(
			MessageHandler.SEND_MESSAGE_API,
			channel=message.channel.name,
			text=message.message,
			username=user.name,
			icon_emoji=user.avatar
		)

	def start_realtime_connection(self):
		return self.client.rtm_connect()

