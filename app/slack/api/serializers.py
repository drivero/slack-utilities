from rest_framework import serializers

from slack.models import Channel, Message


class ChannelSerializer(serializers.ModelSerializer):
	class Meta:
		model = Channel
		fields = (
			'name',
		)


class MessageSerializer(serializers.ModelSerializer):
	channel = ChannelSerializer()

	class Meta:
		model = Message
		fields = (
			'message',
			'date',
			'channel',
		)