from django.conf.urls import url

from .views import BotView, MessageBot

urlpatterns = [
	url(r'^$', BotView.as_view(), name='bot'),
	url(r'/send-message', MessageBot.as_view(), name='send_message'),
]