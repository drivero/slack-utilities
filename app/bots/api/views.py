from rest_framework import status
from rest_framework.response import Response
from rest_framework.generics import ListCreateAPIView
from rest_framework.views import APIView

from .serializers import BotSerializer
from bots.models import Bot
from slack.models import Channel


class BotView(ListCreateAPIView):
	serializer_class = BotSerializer
	queryset = Bot.objects.all()


class MessageBot(APIView):
	def post(self, request):
		bot = Bot.objects.get(name=request.data['bot'])
		channel = Channel.objects.get(name=request.data['channel'])
		bot.send_message(
			request.data['message'],
			channel
		)
		return Response({
			'status': status.HTTP_200_OK,
			'message': request.data['message']
		})
