from rest_framework import serializers

from bots.models import Bot


class BotSerializer(serializers.ModelSerializer):
	class Meta:
		model = Bot
		fields = (
			'id',
			'name',
			'avatar',
			'token',
		)