from django.db import models

from slack.models import Message
from slack.messages import MessageHandler

class Bot(models.Model):
	'''
		A Bot model represents a slack bot with the follow rules:
		- Behavior (A set of commands):
			- Messaging
				- Put a comment 
					- Programmately
					- Based on WSS status
				- Response (ex. @bot Do you have... ?)

			- Commands:
				- Name
				- Command statement (Related to slack commands)

			- AI Commands:
				(Explore this point)
				WSS listening + messages/commands + knowledge building in realtime
	'''
	name = models.CharField(max_length=75)
	avatar = models.CharField(max_length=75)
	description = models.TextField(max_length=255)
	token = models.CharField(max_length=255, unique=True)

	def __str__(self):
		return self.name

	def send_message(self, text, channel):
		message = Message.objects.create(
			message=text,
			channel=channel
		)
		handler = MessageHandler(self.token)
		return handler.send_message(message, self)



