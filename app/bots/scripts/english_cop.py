import requests
import time
from datetime import datetime as dt

from bots.models import Bot
from slack.models import Channel
from slack.messages import MessageHandler


class EnglishCop():
	API_LANG_DETECTION_URL = 'http://ws.detectlanguage.com/0.2/detect'
	API_LANG_DETECTION_TOKEN = 'babb0aa81bcfbae8209e97036ccf17d8'
	LANG_ALLOWED = ['en']
	TOKEN = 'xoxb-49514192519-vifw2ddvd7iAZ67bXCc9fkEF'
	AVATAR = ':cop:'
	CHANNEL = 'util-group'

	def __init__(self):
		self.bot = self._get_bot()
		self.slack = MessageHandler(EnglishCop.TOKEN)
		self.channel = Channel.objects.get(name=EnglishCop.CHANNEL)
		self._start_rtm()

	def _get_bot(self):
		if Bot.objects.filter(name='english-cop').count() == 0:
			description = 'Control the law and the order'
			'on Thursdays'
			'for sophilabers who inflict the law' 
			'and don\'t want to speak in english.'

			Bot.objects.create(
				name='english-cop',
				description=description,
				avatar=EnglishCop.AVATAR,
				token=EnglishCop.TOKEN
			)
		return Bot.objects.get(name='english-cop')

	def _start_rtm(self):
		if self.slack.start_realtime_connection():
			while True:
				rtm_info_res = self.slack.client.rtm_read()
				if len(rtm_info_res) > 0:
					rtm_info = rtm_info_res.pop()
					if 'text'in rtm_info:
						self._punish_conversation(rtm_info['text'])
				time.sleep(1)

	def _punish_conversation(self, message):
		lang = self._detect_language(message)
		if lang not in EnglishCop.LANG_ALLOWED and dt.now().weekday() == 0:
			self.bot.send_message(
				'Today is english day! You should speak in english.',
				self.channel
			)

	def _detect_language(self, message):
		q = message.replace(' ','+')
		url = '{url}?q={query}&key={key}'.format(
			url=EnglishCop.API_LANG_DETECTION_URL,
			query=q,
			key=EnglishCop.API_LANG_DETECTION_TOKEN
		)

		response = requests.get(url)
		return response.json()['data']['detections'].pop()['language']
