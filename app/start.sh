#!/bin/bash

rm db.sqlite3
python manage.py makemigrations
python manage.py migrate
./manage.py runserver
# echo "from django.contrib.auth.models import User; User.objects.create_superuser('admin', 'admin@example.com', 'admin1111')" | python manage.py shell
